FROM node:carbon

# Install software 
RUN apt-get install -y git
RUN npm install forever -g
VOLUME /pause-service

RUN git clone https://bitbucket.org/alari777/pause-service.git /pause-service/

# Create app directory
WORKDIR /pause-service

EXPOSE 80
CMD git pull origin master && npm install -y && npm start