require('dotenv').config();

const Redis = require('ioredis');
const redissub = new Redis({
    port: process.env.PREDIS_PORT,
    host: process.env.PREDIS_HOST,
    password: process.env.PREDIS_PASSWORD,
    db: process.env.PREDIS_DB,
});

const redispub = new Redis({
    port: process.env.PREDIS_PORT,
    host: process.env.PREDIS_HOST,
    password: process.env.PREDIS_PASSWORD,
    db: process.env.PREDIS_DB,
});

const express = require('express');

const app = express();

const server = require('http').Server(app);

const io = require('socket.io')(server);

server.listen(80);


redissub.on('message', (channel, message) => {
    console.log(channel, message);

    if (channel === 're-auth-pause') {
        const json = JSON.parse(message);
        console.log('re-auth-pause:', json, 'Json:', typeof json, 'Data:', typeof message);
        redispub.publish(`p_${json.Id}`, json.Status);
    }
});

io.on('connection', (socket) => {
    console.log('Connected.');

    socket.emit('set-pause', { message: 'set-pause', id: socket.id });

    socket.on('psdata', (data) => {
        console.log('pause data #1:', 'Data:', typeof data);

        redissub.subscribe('start', (err, count) => {});

        redissub.subscribe('re-auth-pause', (err, count) => {});

        if (typeof data === 'string') {
            const json = JSON.parse(data);
            console.log('pause data #2:', json, 'Json:', typeof json);
            console.log('pause data #3:', typeof json.Id, typeof json.Status);
            redispub.publish(`p_${json.Id}`, json.Status);
        }
        if (typeof data === 'object') {
            console.log('pause data #2:', data, 'Json:', typeof data);
            console.log('pause data #3:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            redispub.publish(`p_${data.Id}`, data.Status);
        }
    });

    socket.on('updatesettings', (data) => {
        console.log('update-settings #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('update-settings #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`upse_${data.Id}`, data.Action);
        }
    });

    socket.on('updatesettingsdelta', (data) => {
        console.log('update-settings-delta #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('update-settings-delta #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`upsedelta_${data.Id}`, data.Action);
        }
    });

    socket.on('alignmentvialm', (data) => {
        console.log('alignment-via-lm #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('alignment-via-lm #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`alignmentdelta_${data.Id}`, data.Action);
        }
    });

    socket.on('alignmentvialmtv', (data) => {
        console.log('alignment-via-lm #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('alignment-via-lm #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`alignmentdeltatv_${data.Id}`, data.Action);
        }
    });

    socket.on('quietrestart', (data) => {
        console.log('quiet-restart #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('update-settings #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`upreboot_${data.Id}`, data.Action);
        }
    });

    socket.on('updatecustomsettings', (data) => {
        console.log('update-custom-settings #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('update-settings #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`upsecg_${data.Id}`, data.Action);
        }
    });

    socket.on('hedge', (data) => {
        console.log('hedge #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('hedge #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`uphedge_${data.Id}`, data.Action);
        }
    });

    socket.on('swap', (data) => {
        console.log('swap #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('swap #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`upswap_${data.Id}`, data.Action);
        }
    });

    socket.on('prepair', (data) => {
        console.log('prepairpause #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('prepairpause #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`upprep_${data.Id}`, data.Action);
        }
    });

    // Delta, Trading --- Home

    // Delta
    socket.on('updatesettingsdeltaDelta', (data) => {
        console.log('update-settings-delta #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('update-settings-delta #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`deltaupsedelta_${data.Id}`, data.Action);
        }
    });

    socket.on('alignmentvialmDelta', (data) => {
        console.log('alignment-via-lm #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('alignment-via-lm #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`deltaalignmentdelta_${data.Id}`, data.Action);
        }
    });

    socket.on('alignmentvialmtvDelta', (data) => {
        console.log('alignmentvialmtvDelta:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('alignmentvialmtvDelta #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`deltaalignmentdeltatv_${data.Id}`, data.Action);
        }
    });

    // Trading
    socket.on('updatesettingsdeltaTrading', (data) => {
        console.log('update-settings-trading #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('update-settings-trading #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`tradingupsedelta_${data.Id}`, data.Action);
        }
    });

    socket.on('alignmentvialmTrading', (data) => {
        console.log('alignmentvialmTrading #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('alignmentvialmTrading #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`tradingalignmentdelta_${data.Id}`, data.Action);
        }
    });

    socket.on('alignmentvialmtvTrading', (data) => {
        console.log('alignmentvialmtvTrading #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('alignmentvialmtvTrading #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`tradingalignmentdeltatv_${data.Id}`, data.Action);
        }
    });

    socket.on('tradingPause', (data) => {
        console.log('Pause #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('Pause #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`tradingpause_${data.Id}`, data.Action);
        }
    });

    socket.on('tradingUnPause', (data) => {
        console.log('UnPause #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('UnPause #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`tradingunpause_${data.Id}`, data.Action);
        }
    });

    socket.on('emergencyRestartDelta', (data) => {
        console.log('Emergency Restart Delta #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('Emergency Restart Delta #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`emergencyrestartdelta_${data.Id}`, data.Action);
        }
    });

    socket.on('emergencyRestartTrading', (data) => {
        console.log('Emergency Restart Trading #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('Emergency Restart Trading #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`emergencyrestarttrading_${data.Id}`, data.Action);
        }
    });

    socket.on('updateOptionsV1Settings', (data) => {
        console.log('Update Options V1 Settings #1:', 'Data:', data, 'Typeof:', typeof data);

        if (typeof data === 'object') {
            console.log('Update Options V1 Settings #2:', typeof data.Id, typeof data.Status);
            console.log(data.Id);
            console.log(data.Status);
            console.log(data.Action);
            redispub.publish(`up_settings_options_v1_${data.Id}`, data.Action);
        }
    });

    // Delta, Trading --- End

});